package csf.jamesEtCharles.demineur.controllers;

import ca.csf.simpleFx.SimpleFXController;
import ca.csf.simpleFx.dialogs.SimpleFXDialogChoiceSet;
import ca.csf.simpleFx.dialogs.SimpleFXDialogIcon;
import ca.csf.simpleFx.dialogs.SimpleFXDialogResult;
import ca.csf.simpleFx.dialogs.SimpleFXDialogs;
import ca.csf.simpleFx.events.WindowFocusEvent;
import csf.jamesEtCharles.demineur.dialog.AboutDialog;
import csf.jamesEtCharles.demineur.dialog.HelpDialog;
import csf.jamesEtCharles.demineur.dialog.ScoreDialog;
import csf.jamesEtCharles.demineur.listeners.TileListener;
import csf.jamesEtCharles.demineur.listeners.TileUIListener;
import csf.jamesEtCharles.demineur.model.Difficulty;
import csf.jamesEtCharles.demineur.model.GameBoard;
import csf.jamesEtCharles.demineur.model.Score;
import csf.jamesEtCharles.demineur.model.Tile;
import csf.jamesEtCharles.demineur.ui.GameBoardUI;
import csf.jamesEtCharles.demineur.utils.OSCheck;
import csf.jamesEtCharles.demineur.utils.Utils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Window;
import javafx.util.Duration;

public class GameController extends SimpleFXController implements TileUIListener, TileListener {
    private GameBoard gameBoard;
    private Difficulty difficulty;
    private GameBoardUI gameGrid;
    private int minesCountRemaining;
    private boolean isNewGame;
    private boolean isGameLost;
    private boolean isGameWin;
    private Timeline timeline_timer;
    private int timer_second;

    @FXML
    private MenuBar gameMenu;
    @FXML
    private Pane pane_gameboardui_container;
    @FXML
    private TextField txt_minesCount;
    @FXML
    private TextField txt_timer;
    @FXML
    private Button bt_smile;

    @Override
    public void onLoadedStage() {
        this.getSimpleFxStage().setOnFocusChanged(on_window_focus_changed);
        timeline_timer = new Timeline();
        timeline_timer.setCycleCount(Animation.INDEFINITE);
        timeline_timer.getKeyFrames().add(on_timeline_timer_tick);
        startNewGameWithDifficulty(Difficulty.EASY);
    }

    public EventHandler<WindowFocusEvent> on_window_focus_changed = new EventHandler<WindowFocusEvent>() {
        @Override
        public void handle(WindowFocusEvent event) {
            if (!isGameLost && !isNewGame && !isGameWin) {
                if (!((Window) event.getSource()).isFocused()) {
                    GameController.this.timeline_timer.pause();
                } else {
                    GameController.this.timeline_timer.play();
                }
            }
        }
    };

    public void startNewGameWithDifficulty(Difficulty difficulty) {
        this.timeline_timer.stop();
        this.difficulty = difficulty;
        this.txt_timer.setText("00:00:00");
        if (difficulty == Difficulty.EASY) {
            minesCountRemaining = 10;
            gameBoard = new GameBoard(9, 9, minesCountRemaining);
        } else if (difficulty == Difficulty.MEDIUM) {
            minesCountRemaining = 40;
            gameBoard = new GameBoard(16, 16, minesCountRemaining);
        } else if (difficulty == Difficulty.EXPERT) {
            minesCountRemaining = 99;
            gameBoard = new GameBoard(30, 16, minesCountRemaining);
        }
        isNewGame = true;
        isGameWin = false;
        isGameLost = false;
        timer_second = 0;

        if (gameGrid != null) {
            pane_gameboardui_container.getChildren().remove(gameGrid);
        }

        //Create GridView
        gameGrid = new GameBoardUI(gameBoard);
        pane_gameboardui_container.getChildren().add(gameGrid);

        //Add Listeners
        gameGrid.addTilesUIListener(this);
        gameBoard.addTileListener(this);

        //Set mines count
        this.txt_minesCount.setText(String.valueOf(minesCountRemaining));

        //Set Smiley
        this.bt_smile.setGraphic(new ImageView(new Image(GameController.class.getResource("/csf/jamesEtCharles/demineur/resources/smile_smile.png").toExternalForm())));

        //Resize Window
        resize();
    }

    private KeyFrame on_timeline_timer_tick = new KeyFrame(Duration.seconds(1),
            new EventHandler<ActionEvent>() {
                public void handle(ActionEvent event) {
                    txt_timer.setText(Utils.formatTime(++timer_second));
                }
            }
    );

    private void resize() {
        if (OSCheck.getOperatingSystemType() == OSCheck.OSType.Windows) {
            this.getSimpleFxStage().getInternalJavaFXStage().sizeToScene();
        } else {
            this.getSimpleFxStage().getInternalJavaFXStage().setWidth(gameBoard.getTileXCount() * gameGrid.TILE_WIDTH + 5);
            this.getSimpleFxStage().getInternalJavaFXStage().setHeight(gameBoard.getTileYCount() * gameGrid.TILE_HEIGHT + 85);
        }
    }

    @Override
    public void onTileUIClicked(Tile clickedTile, boolean primaryButton) {
        if (!isGameLost) {
            if (primaryButton) {
                clickedTile.setClicked();
            } else {
                if (!isNewGame) {
                    clickedTile.switchMarker();
                    if (clickedTile.getMarker() == Tile.Marker.FLAG) {
                        minesCountRemaining--;
                    } else if (clickedTile.getMarker() == Tile.Marker.QUESTION_MARK) {
                        minesCountRemaining++;
                    }
                    this.txt_minesCount.setText(String.valueOf(minesCountRemaining));
                }
            }
        }
    }

    @Override
    public void onTileChanged(Tile tile, boolean silent) {
        if (isNewGame) {
            isNewGame = false;
            timeline_timer.play();
        }

        if (tile.isMine() && tile.isClicked() && !silent) {
            isGameLost = true;
            timeline_timer.stop();
            gameBoard.revealAllMines();
            gameBoard.setAllWrongFlaggedTiles();
            this.bt_smile.setGraphic(new ImageView(new Image(GameController.class.getResource("/csf/jamesEtCharles/demineur/resources/smile_dead.png").toExternalForm())));
            SimpleFXDialogs.showMessageBox("Vous avez perdu!", "Ah... c'est pas de chance.", SimpleFXDialogIcon.ERROR, SimpleFXDialogChoiceSet.OK, SimpleFXDialogResult.OK, this.getSimpleFxStage());
        }

        if (!tile.isMine() && gameBoard.isGameWin()) {
            isGameWin = true;
            timeline_timer.stop();
            gameBoard.setAllMinesFlagged();
            gameBoard.setAllWrongFlaggedTiles();
            this.bt_smile.setGraphic(new ImageView(new Image(GameController.class.getResource("/csf/jamesEtCharles/demineur/resources/smile_happy.png").toExternalForm())));

            Score score = new Score(this.difficulty.toString(), String.valueOf(timer_second), "");
            SimpleFXDialogs.showMessageBox("Vous avez Gagné!", "Wow! Ça n'arrive pas souvent!", SimpleFXDialogIcon.INFORMATION, SimpleFXDialogChoiceSet.OK, SimpleFXDialogResult.OK, this.getSimpleFxStage());
            if (Utils.isScoreBetter(score)) {
                SimpleFXDialogResult result = SimpleFXDialogs.showMessageBox("Vous avez battu un record.", "Souhaitez-vous enregistrer votre score ?", SimpleFXDialogIcon.QUESTION, SimpleFXDialogChoiceSet.YES_NO, SimpleFXDialogResult.NO, this.getSimpleFxStage());
                if (result == SimpleFXDialogResult.YES) {
                    String nom = SimpleFXDialogs.showInputBox("Quel est votre nom?", "Entrez votre nom.", this.getSimpleFxStage());
                    if (nom == null || nom.isEmpty()) {
                        nom = "Anonymous";
                    }
                    score.setName(nom);
                    Utils.writeScore(score);
                }
            }
        }
    }

    @FXML
    private void on_item_cheat_click(ActionEvent actionEvent) {
        gameBoard.revealAllMines();
    }

    //It's so ugly...

    @FXML
    private void bt_smile_click(ActionEvent actionEvent) {
        startNewGameWithDifficulty(this.difficulty);
    }

    @FXML
    private void on_item_quit_click(ActionEvent actionEvent) {
        System.exit(0);
    }

    @FXML
    private void on_item_easy_click(ActionEvent actionEvent) {
        startNewGameWithDifficulty(Difficulty.EASY);
    }

    @FXML
    private void on_item_medium_click(ActionEvent actionEvent) {
        startNewGameWithDifficulty(Difficulty.MEDIUM);
    }

    @FXML
    private void on_item_expert_click(ActionEvent actionEvent) {
        startNewGameWithDifficulty(Difficulty.EXPERT);
    }

    @FXML
    private void on_item_new_game_click(ActionEvent actionEvent) {
        startNewGameWithDifficulty(difficulty);
    }

    @FXML
    private void on_item_help_click(ActionEvent actionEvent) {
        HelpDialog helpDialog = new HelpDialog(this.getSimpleFxStage().getInternalJavaFXStage());
        helpDialog.show();
    }

    @FXML
    private void on_item_about_click(ActionEvent actionEvent) {
        AboutDialog aboutDialog = new AboutDialog(this.getSimpleFxStage().getInternalJavaFXStage());
        aboutDialog.show();
    }

    @FXML
    private void on_item_meilleurs_scores_click(ActionEvent actionEvent) {
        ScoreDialog scoreDialog = new ScoreDialog(this.getSimpleFxStage().getInternalJavaFXStage());
        scoreDialog.show();
    }
}