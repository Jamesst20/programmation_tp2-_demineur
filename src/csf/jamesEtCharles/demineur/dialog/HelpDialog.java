package csf.jamesEtCharles.demineur.dialog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class HelpDialog {
    private Stage dialogStage;

    public HelpDialog(Stage parentStage) {
        this.dialogStage = new Stage();
        this.dialogStage.setTitle("Aide");

        BorderPane root = new BorderPane();

        Scene sceneDialog = new Scene(root);

        this.dialogStage.initModality(Modality.WINDOW_MODAL);
        this.dialogStage.initOwner(parentStage);
        this.dialogStage.setScene(sceneDialog);

        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(HelpDialog.class.getResource("help.html").toExternalForm());

        root.setCenter(webView);

        Button bt_ok = new Button("Ok");

        bt_ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                HelpDialog.this.dialogStage.close();
            }
        });

        HBox row = new HBox(bt_ok);
        row.setAlignment(Pos.CENTER);

        root.setBottom(row);
    }

    public void show() {
        this.dialogStage.showAndWait();
    }
}
