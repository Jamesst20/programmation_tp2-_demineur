package csf.jamesEtCharles.demineur.dialog;

import csf.jamesEtCharles.demineur.model.Difficulty;
import csf.jamesEtCharles.demineur.model.Score;
import csf.jamesEtCharles.demineur.utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.IOException;
import java.util.List;

/**
 * Created by James on 2014-10-30.
 */
public class ScoreDialog {
    private Stage dialogStage;

    @FXML
    private Label lbl_temps_facile;
    @FXML
    private Label lbl_temps_intermediaire;
    @FXML
    private Label lbl_temps_expert;
    @FXML
    private Label lbl_nom_facile;
    @FXML
    private Label lbl_nom_intermediaire;
    @FXML
    private Label lbl_nom_expert;

    public ScoreDialog(Stage parentStage) {
        try {
            this.dialogStage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(ScoreDialog.class.getResource("/csf/jamesEtCharles/demineur/dialog/ScoreDialog.fxml"));
            //FXMLLoader#load will create a new instance of ScoreDialog (its controller declared in fxml). We don't want that.
            fxmlLoader.setControllerFactory(new Callback<Class<?>, Object>() {
                @Override
                public Object call(Class<?> param) {
                    return ScoreDialog.this;
                }
            });
            Parent root = fxmlLoader.load();
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.setScene(new Scene(root));
            dialogStage.initOwner(parentStage);
            dialogStage.initModality(Modality.APPLICATION_MODAL);

            //Load scores
            List<Score> scores = Utils.readScores();
            for (Score score : scores) {
                if (score.getDifficulty().equalsIgnoreCase(Difficulty.EASY.toString())) {
                    lbl_nom_facile.setText(score.getName());
                    lbl_temps_facile.setText(score.getTime() + " secondes");
                } else if (score.getDifficulty().equalsIgnoreCase(Difficulty.MEDIUM.toString())) {
                    lbl_nom_intermediaire.setText(score.getName());
                    lbl_temps_intermediaire.setText(score.getTime() + " secondes");
                } else if (score.getDifficulty().equalsIgnoreCase(Difficulty.EXPERT.toString())) {
                    lbl_nom_expert.setText(score.getName());
                    lbl_temps_expert.setText(score.getTime() + " secondes");
                }
            }
        } catch (IOException e) {
            System.err.println("Failed to create ScoreDialog dialog");
            e.printStackTrace();
        }
    }

    public void show() {
        dialogStage.show();
    }

    public void showAndWait() {
        dialogStage.showAndWait();
    }

    @FXML
    public void on_bt_erase_scores_click(ActionEvent actionEvent) {
        Utils.eraseScores();

        lbl_nom_facile.setText("N/A");
        lbl_temps_facile.setText("N/A");
        lbl_nom_intermediaire.setText("N/A");
        lbl_temps_intermediaire.setText("N/A");
        lbl_nom_expert.setText("N/A");
        lbl_temps_expert.setText("N/A");

    }

    @FXML
    public void on_bt_ok_click(ActionEvent actionEvent) {
        this.dialogStage.close();
    }
}
