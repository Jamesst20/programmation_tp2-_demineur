package csf.jamesEtCharles.demineur.dialog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by James on 2014-11-11.
 */
public class AboutDialog {
    private Stage dialogStage;

    public AboutDialog(Stage parentStage) {
        this.dialogStage = new Stage();
        this.dialogStage.setTitle("A propos de");

        BorderPane root = new BorderPane();

        Scene sceneDialog = new Scene(root);

        this.dialogStage.initModality(Modality.WINDOW_MODAL);
        this.dialogStage.initOwner(parentStage);
        this.dialogStage.setScene(sceneDialog);

        VBox center = new VBox();
        root.setCenter(center);

        ImageView imageDemineur = new ImageView();
        imageDemineur.setImage(new Image(HelpDialog.class.getResource("/csf/jamesEtCharles/demineur/resources/demineur.png").toExternalForm()));
        imageDemineur.setPreserveRatio(true);
        imageDemineur.setFitHeight(225.0);
        HBox rowImage = new HBox(imageDemineur);
        center.getChildren().add(imageDemineur);

        Label label_par = new Label("Par");
        Label label_james = new Label("James St-Pierre");
        Label label_charles = new Label("Charles-Edouard Beaudet");

        HBox row1_label = new HBox(label_par);
        HBox row2_label = new HBox(label_james);
        HBox row3_label = new HBox(label_charles);
        row1_label.setAlignment(Pos.CENTER);
        row2_label.setAlignment(Pos.CENTER);
        row3_label.setAlignment(Pos.CENTER);
        center.getChildren().add(row1_label);
        center.getChildren().add(row2_label);
        center.getChildren().add(row3_label);


        Button bt_ok = new Button("Ok");

        bt_ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AboutDialog.this.dialogStage.close();
            }
        });

        HBox row = new HBox(bt_ok);
        row.setAlignment(Pos.CENTER);

        root.setBottom(row);
    }

    public void show() {
        this.dialogStage.showAndWait();
    }
}
