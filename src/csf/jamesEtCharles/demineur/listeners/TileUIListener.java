package csf.jamesEtCharles.demineur.listeners;

import csf.jamesEtCharles.demineur.model.Tile;

/**
 * Created by james on 14-10-10.
 */
public interface TileUIListener {
    public void onTileUIClicked(Tile clickedTile, boolean primaryButton);
}
