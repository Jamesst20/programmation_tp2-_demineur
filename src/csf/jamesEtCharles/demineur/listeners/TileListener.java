package csf.jamesEtCharles.demineur.listeners;

import csf.jamesEtCharles.demineur.model.Tile;

public interface TileListener {
    public void onTileChanged(Tile tile, boolean silent);
}
