package csf.jamesEtCharles.demineur.model;

import csf.jamesEtCharles.demineur.listeners.TileListener;
import csf.jamesEtCharles.demineur.utils.Point2D;
import csf.jamesEtCharles.demineur.utils.Point2DRandom;

public class GameBoard implements TileListener {
    private final static int MINIMUM_EMPTY_TILES_FIRST_CLICK = 2;
    private final Tile[][] tiles;
    private final int numberOfMine;
    private final int dimensionX;
    private final int dimensionY;
    private boolean isBoardNew;

    public GameBoard(int dimensionX, int dimensionY, int numberOfMine) {
        this.dimensionX = dimensionX;
        this.dimensionY = dimensionY;
        this.numberOfMine = numberOfMine;
        this.isBoardNew = true;

        this.tiles = new Tile[dimensionX][dimensionY];

        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                tiles[x][y] = new Tile();
                tiles[x][y].addTileListener(this);
            }
        }
    }

    public void addTileListener(TileListener listener) {
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                tiles[x][y].addTileListener(listener);
            }
        }
    }

    private void placeMines(int minesCount, Tile firstTileExcluded) {
        int minesPlaced = 0;

        Point2D firstTileExcludedPoint = getTileCoordinates(firstTileExcluded);

        //Create point generator
        Point2DRandom random = new Point2DRandom();
        //Add whole grid as possible random values
        random.addRange(new Point2D(0, 0), new Point2D(dimensionX - 1, dimensionY - 1));

        if (firstTileExcluded != null) {
            //Remove points to exclude to make sure first click only has empty tile
            for (int x = firstTileExcludedPoint.x - MINIMUM_EMPTY_TILES_FIRST_CLICK; x <= firstTileExcludedPoint.x + MINIMUM_EMPTY_TILES_FIRST_CLICK; x++) {
                for (int y = firstTileExcludedPoint.y - MINIMUM_EMPTY_TILES_FIRST_CLICK; y <= firstTileExcludedPoint.y + MINIMUM_EMPTY_TILES_FIRST_CLICK; y++) {
                    random.removePoint2D(new Point2D(x, y));
                }
            }
        }

        //Create mines..
        while (minesPlaced < minesCount) {
            Point2D randomPoint = random.getRandomPoint2D();
            int x = randomPoint.x;
            int y = randomPoint.y;

            if (!tiles[x][y].isMine() && tiles[x][y] != firstTileExcluded) {
                tiles[x][y].setMine(true);
                minesPlaced++;
            }
        }

    }

    public int getTileYCount() {
        return dimensionY;
    }

    public int getTileXCount() {
        return dimensionX;
    }

    public Tile getTileAt(int x, int y) {
        return tiles[x][y];
    }

    private void popEmptyTilesRecursive(int x, int y) {
        if (tiles[x][y].getNearMinesCount() != 0) {
            return;
        }
        tiles[x][y].setClickedSilent();

        //Open non-empty tiles around this empty tile
        for (int X = -1; X <= 1; X++) {
            for (int Y = -1; Y <= 1; Y++) {
                if (x + X >= 0 && y + Y >= 0 && x + X < dimensionX && y + Y < dimensionY) {
                    if (!tiles[x + X][y + Y].isClicked() && !tiles[x + X][y + Y].isMine() && tiles[x + X][y + Y].getNearMinesCount() != 0) {
                        tiles[x + X][y + Y].setClickedSilent();
                    }
                }
            }
        }

        //Search recursively other nearby empty tiles
        for (int X = -1; X <= 1; X++) {
            for (int Y = -1; Y <= 1; Y++) {
                if (x + X >= 0 && y + Y >= 0 && x + X < dimensionX && y + Y < dimensionY) {
                    if (!tiles[x + X][y + Y].isClicked()) {
                        popEmptyTilesRecursive(x + X, y + Y);
                    }
                }
            }
        }
    }

    private void setNearMinesCount() {
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                setNearMinesCountAt(x, y);
            }
        }
    }

    private void setNearMinesCountAt(int X, int Y) {
        int minesFound = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (X + x >= 0 && X + x <= dimensionX - 1 && Y + y >= 0 && Y + y <= dimensionY - 1) {
                    if (tiles[X + x][Y + y].isMine()) {
                        minesFound++;
                    }
                }
            }
        }
        tiles[X][Y].setNearMines(minesFound);
    }

    private Point2D getTileCoordinates(Tile tile) {
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                if (tiles[x][y] == tile) {
                    return new Point2D(x, y);
                }
            }
        }
        return null;
    }

    public boolean isGameWin() {
        int goodClickedTiles = 0;

        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                if (tiles[x][y].isClicked() && !tiles[x][y].isMine()) {
                    goodClickedTiles++;
                }
            }
        }
        return goodClickedTiles == (dimensionX * dimensionY) - numberOfMine;
    }

    public void setAllWrongFlaggedTiles() {
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                if (!tiles[x][y].isMine() && tiles[x][y].getMarker() == Tile.Marker.FLAG) {
                    tiles[x][y].setWrongFlagMarker();
                }
            }
        }
    }

    public void setAllMinesFlagged() {
        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                if (tiles[x][y].isMine()) {
                    while (tiles[x][y].getMarker() != Tile.Marker.FLAG) {
                        tiles[x][y].switchMarker();
                    }
                    tiles[x][y].setFlagMarker();
                }
            }
        }
    }

    public void revealAllMines() {
        if (isBoardNew) {
            placeMines(this.numberOfMine, null);
            setNearMinesCount();
            isBoardNew = false;
        }

        for (int x = 0; x < dimensionX; x++) {
            for (int y = 0; y < dimensionY; y++) {
                if (tiles[x][y].isMine()) {
                    tiles[x][y].setMineMarker();
                }
            }
        }
    }

    @Override
    public void onTileChanged(Tile tile, boolean silent) {
        if (tile.isClicked()) {
            if (isBoardNew) {
                //Make sure first click always open few empty tiles
                placeMines(this.numberOfMine, tile);
                setNearMinesCount();
                isBoardNew = false;
            }

            if (!silent) {
                if (tile.getNearMinesCount() == 0) {
                    //Open all nearby empty tiles
                    Point2D clickedTile = getTileCoordinates(tile);
                    popEmptyTilesRecursive(clickedTile.x, clickedTile.y);
                }
            }
        }
    }
}