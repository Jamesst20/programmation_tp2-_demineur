package csf.jamesEtCharles.demineur.model;

/**
 * Created by James on 2014-10-30.
 */
public class Score {
    public final String time;
    public final String difficulty;
    public String name;

    public Score(String difficulty, String time, String name) {
        this.time = time;
        this.difficulty = difficulty;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getTime() {
        return time;
    }

    public void setName(String name) {
        this.name = name;
    }
}
