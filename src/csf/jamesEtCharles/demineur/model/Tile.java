package csf.jamesEtCharles.demineur.model;

import csf.jamesEtCharles.demineur.listeners.TileListener;

import java.util.ArrayList;
import java.util.List;

public class Tile {
    private boolean isMine;
    private int nearMinesCount;
    private boolean clicked = false;
    private List<TileListener> listeners = new ArrayList<>();
    private Marker marker = Marker.NONE;

    public Tile() {
    }

    public void setNearMines(int nearMines) {
        this.nearMinesCount = nearMines;
    }

    public int getNearMinesCount() {
        return this.nearMinesCount;
    }

    public boolean isMine() {
        return this.isMine;
    }

    public void setMine(boolean mine) {
        this.isMine = mine;
    }

    public void setClicked() {
        if (this.isClicked() || !(this.marker == Marker.NONE || this.marker == Marker.MINE)) {
            return;
        }
        this.clicked = true;
        this.marker = isMine() ? Marker.MINE : Marker.NONE;

        fireOnTileChanged(false);
    }

    public void setClickedSilent() {
        if (this.isClicked()) {
            return;
        }

        this.clicked = !isMine() || this.marker == Marker.FLAG;
        this.marker = isMine() ? (this.marker != Marker.FLAG ? Marker.MINE : Marker.FLAG) : Marker.NONE;

        fireOnTileChanged(true);
    }

    public void setMineMarker() {
        if (!this.isClicked() && this.isMine() && this.marker != Marker.MINE) {
            this.marker = Marker.MINE;
            fireOnTileChanged(false);
        }
    }

    public void setFlagMarker() {
        if (!this.isClicked() && this.marker != Marker.FLAG) {
            this.marker = Marker.FLAG;
            fireOnTileChanged(false);
        }
    }

    public boolean isClicked() {
        return this.clicked;
    }

    public void addTileListener(TileListener listener) {
        this.listeners.add(listener);
    }

    private void fireOnTileChanged(boolean silent) {
        for (TileListener listener : listeners) {
            listener.onTileChanged(this, silent);
        }
    }

    public void switchMarker() {
        if (!this.clicked) {
            if (this.marker == Marker.NONE) {
                this.marker = Marker.FLAG;
            } else if (this.marker == Marker.FLAG) {
                this.marker = Marker.QUESTION_MARK;
            } else if (this.marker == Marker.MINE) {
                this.marker = Marker.FLAG;
            } else if (this.marker == Marker.QUESTION_MARK) {
                this.marker = Marker.NONE;
            }
            fireOnTileChanged(false);
        }
    }

    public void setWrongFlagMarker() {
        if (!this.isClicked()) {
            this.marker = Marker.WRONG_FLAG;
            fireOnTileChanged(false);
        }
    }

    public Marker getMarker() {
        return this.marker;
    }

    public enum Marker {
        FLAG, QUESTION_MARK, WRONG_FLAG, MINE, NONE;
    }

}