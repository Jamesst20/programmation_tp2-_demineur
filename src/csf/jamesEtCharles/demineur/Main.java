package csf.jamesEtCharles.demineur;

import ca.csf.simpleFx.SimpleFXApplication;
import ca.csf.simpleFx.SimpleFXApplicationLauncher;
import ca.csf.simpleFx.SimpleFXScene;
import ca.csf.simpleFx.SimpleFXStage;
import csf.jamesEtCharles.demineur.controllers.GameController;
import javafx.stage.StageStyle;

public class Main extends SimpleFXApplication {

    public static void main(String[] args) {
        SimpleFXApplicationLauncher.startSimpleFXApplication(Main.class, args);
    }

    public void start() {
        try {
            SimpleFXScene simpleFXScene = new SimpleFXScene(Main.class.getResource("/csf/jamesEtCharles/demineur/ui/Game.fxml"), Main.class.getResource("/csf/jamesEtCharles/demineur/application.css"), new GameController());
            SimpleFXStage simpleFXStage = new SimpleFXStage("Démineur", StageStyle.DECORATED, simpleFXScene, this);
            simpleFXStage.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
