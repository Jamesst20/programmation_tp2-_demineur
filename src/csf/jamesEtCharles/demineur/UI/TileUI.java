package csf.jamesEtCharles.demineur.ui;

import csf.jamesEtCharles.demineur.listeners.TileListener;
import csf.jamesEtCharles.demineur.listeners.TileUIListener;
import csf.jamesEtCharles.demineur.model.Tile;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class TileUI extends ToggleButton implements TileListener {
    private Tile tile;
    private List<TileUIListener> listeners = new ArrayList<>();
    private EventHandler<MouseEvent> on_mouse_click = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            TileUI.this.setSelected(false);
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                fireOnTileUIClicked(true);
            } else if (event.getButton().equals(MouseButton.SECONDARY)) {
                fireOnTileUIClicked(false);
            }
        }
    };

    public TileUI(Tile tile) {
        this.tile = tile;
        this.tile.addTileListener(this);
        this.setOnMouseClicked(on_mouse_click);
    }

    @Override
    public void onTileChanged(Tile tile, boolean silent) {
        if (tile.getMarker() == Tile.Marker.FLAG) {
            ImageView imageView = new ImageView(new Image(getClass().getResource("/csf/jamesEtCharles/demineur/resources/flag.png").toExternalForm(), 20, 20, true, true));
            this.setGraphic(imageView);
        } else if (tile.getMarker() == Tile.Marker.QUESTION_MARK) {
            ImageView imageView = new ImageView(new Image(getClass().getResource("/csf/jamesEtCharles/demineur/resources/interrogation.png").toExternalForm(), 20, 20, true, true));
            this.setGraphic(imageView);
        } else if (tile.getMarker() == Tile.Marker.WRONG_FLAG) {
            ImageView imageView = new ImageView(new Image(getClass().getResource("/csf/jamesEtCharles/demineur/resources/wrongBomb.png").toExternalForm(), 20, 20, true, true));
            this.setGraphic(imageView);
        } else if (tile.getMarker() == Tile.Marker.MINE) {
            ImageView imageView = new ImageView(new Image(getClass().getResource("/csf/jamesEtCharles/demineur/resources/bomb.png").toExternalForm(), 20, 20, true, true));
            this.setGraphic(imageView);
        } else if (tile.getMarker() == Tile.Marker.NONE) {
            this.setGraphic(null);
        }

        if (tile.isClicked()) {
            if (!tile.isMine()) {
                if (tile.getNearMinesCount() != 0) {
                    this.setTextColor(tile);
                    this.setText(String.valueOf(tile.getNearMinesCount()));
                }
            } else {
                this.setStyle("-fx-background-color:red");
            }
            this.setTileUIToggled();
        }
    }

    private void setTextColor(Tile tile) {
        if (!tile.isMine()) {
            //Set Text Color
            switch (tile.getNearMinesCount()) {
                case 1:
                    this.setTextFill(Color.GREEN);
                    break;
                case 2:
                    this.setTextFill(Color.DARKGREEN);
                    break;
                case 3:
                    this.setTextFill(Color.BLUE);
                    break;
                case 4:
                    this.setTextFill(Color.DARKBLUE);
                    break;
                case 5:
                    this.setTextFill(Color.PINK);
                    break;
                case 6:
                    this.setTextFill(Color.HOTPINK);
                    break;
                case 7:
                    this.setTextFill(Color.RED);
                    break;
                case 8:
                    this.setTextFill(Color.DARKRED);
                    break;
                case 9:
                    this.setTextFill(Color.BLACK);
                    break;
            }
        }
    }

    private void setTileUIToggled() {
        //Make sure it stays toggled forever
        this.setSelected(true);
        this.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                TileUI.this.setSelected(true);
            }
        });
    }

    public void addTileUIListener(TileUIListener listener) {
        this.listeners.add(listener);
    }

    private void fireOnTileUIClicked(boolean primaryButton) {
        for (TileUIListener listener : listeners) {
            listener.onTileUIClicked(tile, primaryButton);
        }
    }

}