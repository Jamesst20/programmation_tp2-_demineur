package csf.jamesEtCharles.demineur.ui;

import csf.jamesEtCharles.demineur.listeners.TileUIListener;
import csf.jamesEtCharles.demineur.model.GameBoard;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;


public class GameBoardUI extends GridPane {
    public final static double TILE_WIDTH = 30;
    public final static double TILE_HEIGHT = 30;

    private final GameBoard gameBoard;
    private final List<TileUI> tilesUI = new ArrayList<>();
    private final List<TileUIListener> listeners = new ArrayList<>();

    public GameBoardUI(GameBoard gameBoard) {
        this.gameBoard = gameBoard;

        for (int x = 0; x < gameBoard.getTileXCount(); x++) {
            for (int y = 0; y < gameBoard.getTileYCount(); y++) {
                //Create the new tile ui
                TileUI tileUI = new TileUI(gameBoard.getTileAt(x, y));

                //Add tile to pane and keep a reference to it
                this.tilesUI.add(tileUI);
                this.add(tileUI, x, y);

                //Make sure tiles will always have a the proper size
                tileUI.setMaxSize(TILE_WIDTH, TILE_HEIGHT);
                tileUI.setMinSize(TILE_WIDTH, TILE_HEIGHT);
                tileUI.setPrefSize(TILE_WIDTH, TILE_HEIGHT);
            }
        }
    }

    public void addTilesUIListener(TileUIListener listener) {
        for (TileUI tileUI : tilesUI) {
            tileUI.addTileUIListener(listener);
        }
    }

}