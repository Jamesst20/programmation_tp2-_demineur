package csf.jamesEtCharles.demineur.utils;


public class Point2D {
    public int x;
    public int y;

    public Point2D() {

    }

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void printPoint2D() {
        System.out.println("(" + x + ", " + y + ")");
    }

    @Override
    public boolean equals(Object other) {
        Point2D otherPoint = ((Point2D) other);
        return otherPoint.x == this.x && otherPoint.y == this.y;
    }

    @Override
    public int hashCode() {
        return x * y + y * (x + y) + y * y;
    }
}
