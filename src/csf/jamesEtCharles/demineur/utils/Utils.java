package csf.jamesEtCharles.demineur.utils;

import csf.jamesEtCharles.demineur.model.Score;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static String formatTime(long seconds) {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(day);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.DAYS.toMinutes(day) - TimeUnit.HOURS.toMinutes(hours);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.DAYS.toSeconds(day) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minute);
        return (hours < 10 ? "0" + hours : hours) + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
    }

    public static List<Score> readScores() {
        List<Score> scores = new ArrayList<Score>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Scores.txt"));

            String line;
            while ((line = reader.readLine()) != null) {
                String[] scoreSplitted = line.split(";");
                if (scoreSplitted.length == 3) {
                    scores.add(new Score(scoreSplitted[0], scoreSplitted[1], scoreSplitted[2]));
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("Scores.txt not found");
        } catch (IOException e) {
            System.err.println("Failed to read Scores.txt");
        }
        return scores;
    }

    public static boolean isScoreBetter(Score score) {
        List<Score> scores = readScores();

        boolean found = false;
        boolean scoreBetter = false;

        for (Score savedScore : scores) {
            if (savedScore.getDifficulty().equalsIgnoreCase(score.getDifficulty())) {
                if (Integer.parseInt(savedScore.getTime()) > Integer.parseInt(score.getTime())) {
                    scoreBetter = true;
                }
                found = true;
                break;
            }
        }

        if (!found || scoreBetter) {
            return true;
        }
        return false;
    }

    public static boolean writeScore(Score score) {
        try {
            List<Score> scores = readScores();

            Score toDelete = null;
            boolean found = false;
            for (Score savedScore : scores) {
                if (savedScore.getDifficulty().equalsIgnoreCase(score.getDifficulty())) {
                    if (Integer.parseInt(savedScore.getTime()) > Integer.parseInt(score.getTime())) {
                        toDelete = savedScore;
                    }
                    found = true;
                    break;
                }
            }

            if (!found || toDelete != null) {
                if (toDelete != null) {
                    scores.remove(toDelete);
                }
                scores.add(score);

                BufferedWriter writter = new BufferedWriter(new FileWriter("Scores.txt"));

                for (Score savedScore : scores) {
                    writter.write(savedScore.getDifficulty() + ";" + savedScore.getTime() + ";" + savedScore.getName() + "\n");
                }
                writter.close();
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void eraseScores() {
        File file = new File("Scores.txt");
        file.delete();
    }
}
