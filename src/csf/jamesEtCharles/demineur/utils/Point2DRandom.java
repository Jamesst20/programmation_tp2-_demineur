package csf.jamesEtCharles.demineur.utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;


public class Point2DRandom {
    Set<Point2D> possibleValues = new HashSet<>();

    public Point2DRandom() {

    }

    public void addRange(Point2D from, Point2D to) {
        int minX = Math.min(from.x, to.x);
        int maxX = Math.max(from.x, to.x);
        int minY = Math.min(from.y, to.y);
        int maxY = Math.max(from.y, to.y);

        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                if (x >= 0 && y >= 0) {
                    possibleValues.add(new Point2D(x, y));
                }
            }
        }
    }

    public void addPoint2D(Point2D add) {
        this.possibleValues.add(add);
    }

    public void removePoint2D(Point2D toRemove) {
        this.possibleValues.remove(toRemove);
    }

    public Point2D getRandomPoint2D() {
        return (Point2D) possibleValues.toArray()[new Random().nextInt(possibleValues.size())];
    }

    public int getPossibilitiesCount() {
        return this.possibleValues.size();
    }

    public void printPossibilities() {
        for (Point2D point : possibleValues) {
            point.printPoint2D();
        }
    }
}