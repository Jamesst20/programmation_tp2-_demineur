import csf.jamesEtCharles.demineur.GameBoard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class GameBoardTests {

    private GameBoard gameBoard;

    @Before
    public void setup(){
        gameBoard = new GameBoard(10,10,9);
    }

    @Test
    public void whenNewGameBoardCreated_gameBoardHasRightDimensions(){
        assertEquals(10, gameBoard.getTileXCount());
        assertEquals(10, gameBoard.getTileYCount());
    }

    @Test
    public void whenNewGameBoardCreated_gameBoardIsFullOfTiles(){
        for (int x = 0; x < gameBoard.getTileXCount(); x++){
            for(int y = 0; y < gameBoard.getTileYCount(); y++){
                assertFalse(null == gameBoard.getTileAt(x,y));
            }
        }
    }

    @Test
    public void whenPlacingMines_gameBoardHasSpecifiedNumberOfMines(){
        gameBoard.getTileAt(0,0).setClicked();
        gameBoard.onTileChanged(gameBoard.getTileAt(0,0),false);
        int minesfound = 0;
        for (int x = 0; x < gameBoard.getTileXCount(); x++){
            for(int y = 0; y < gameBoard.getTileYCount(); y++){
                if (gameBoard.getTileAt(x,y).isMine()){
                    minesfound++;
                }
            }
        }
        assertEquals(9, minesfound);
    }
}