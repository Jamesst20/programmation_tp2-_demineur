import csf.jamesEtCharles.demineur.Tile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Charles-Édouard on 2014-10-02.
 */
public class TileTests {

    private Tile tile;

    @Before
    public void setup(){
        tile = new Tile();
    }

    @Test
    public void whenNewTileCreated_tileIsNotClickedAndHisMarkerIsNone(){
        assertEquals(Tile.Marker.NONE, tile.getMarker());
        assertFalse(tile.isClicked());
    }

    @Test
    public void whenSettingIsMineValue_valueIsAdded(){
        assertFalse(tile.isMine());
        tile.setMine(true);
        assertTrue(tile.isMine());
    }

    @Test
    public void whenSettingNearMinesCount_valueIsAdded(){
        assertEquals(0, tile.getNearMinesCount());
        tile.setNearMines(32);
        assertEquals(32, tile.getNearMinesCount());
    }

    @Test
    public void whenMarkerIsSwitched_newMarkerIsRight(){
        assertEquals(Tile.Marker.NONE,tile.getMarker());
        tile.switchMarker();
        assertEquals(Tile.Marker.FLAG, tile.getMarker());
        tile.switchMarker();
        assertEquals(Tile.Marker.QUESTION_MARK, tile.getMarker());
        tile.switchMarker();
        assertEquals(Tile.Marker.NONE, tile.getMarker());
    }

    @Test
    public void whenTileIsClicked_isClickedReturnsTrue(){
        assertFalse(tile.isClicked());
        tile.setClicked();
        assertTrue(tile.isClicked());
    }

    @Test
    public void whenSetMarkerMineWhenTileIsMine_markerIsMine(){
        tile.setMineMarker();
        assertEquals(Tile.Marker.NONE, tile.getMarker());

        tile.setMine(true);
        tile.setMineMarker();
        assertEquals(Tile.Marker.MINE, tile.getMarker());

    }

    @Test
    public void whenSetMarkerWrongFlag_markerIsWrongFlag(){
        tile.setWrongFlagMarker();
        assertEquals(Tile.Marker.WRONG_FLAG,tile.getMarker());
    }


}